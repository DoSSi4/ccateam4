package kz.aitu.it1902.team6.controller;

import kz.aitu.it1902.team6.models.Name;
import kz.aitu.it1902.team6.repository.NameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@RestController()
//@RequestMapping(value = "/w")
public class NamesWriteController {

    @Autowired
    private NameRepository nameRepository;

    @GetMapping("/add-location")
    public String hello(@RequestParam(value = "coord") String name, Model model) {
        Name name1 = new Name(name);
        Name newName = nameRepository.save(name1);;
        return "Your location: " + newName.toString();
    }
 /* localhost:8080/gps-tracking-writer/w/create-name  */
}
