package kz.aitu.it1902.team6.repository;

import kz.aitu.it1902.team6.models.Name;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository

public interface NameRepository extends JpaRepository<Name, Long> {
    List<Name> findByName(String name);
}
