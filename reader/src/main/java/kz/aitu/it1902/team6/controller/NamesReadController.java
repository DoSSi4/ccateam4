package kz.aitu.it1902.team6.controller;

import kz.aitu.it1902.team6.models.Name;
import kz.aitu.it1902.team6.repository.NameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@RestController
//@RequestMapping(value = "/r")
public class NamesReadController {
    @Autowired
    public NameRepository nameRepository;

    @GetMapping("/show-coordinates")
    public String hello() {
        List<Name> Names = nameRepository.findAll();
        String all_names = Names.toString();
        return all_names;
    }

    /* localhost/gps-tracking-reader/r/show-names */
}
